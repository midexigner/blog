const mongoose = require("mongoose");

const CategorySchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    parent: {
        type: String,
        default: "0",
      },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Category", CategorySchema);