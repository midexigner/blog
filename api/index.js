const express = require("express")
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const authRoute = require("./routes/auth");
const userRoute = require("./routes/users");
const postRoute = require("./routes/posts");
const categoryRoute = require("./routes/categories");
const multer = require("multer");
const path = require("path");

const app = express();
dotenv.config();
app.use(express.json())
app.use("/images", express.static(path.join(__dirname, "/images")));

const uri = process.env.MONGO_URL;
// database connection start
main().catch(err => console.log(err));
async function main() {
  await mongoose.connect(uri);
  console.log('Database connection');
}
// database connection end

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, "images");
    },
    filename: (req, file, cb) => {
      cb(null, req.body.name);
    },
  });
  
  const upload = multer({ storage: storage });
  app.post("/api/upload", upload.single("file"), (req, res) => {
    res.status(200).json("File has been uploaded");
  });
  
  app.use("/api/auth", authRoute);
  app.use("/api/users", userRoute);
  app.use("/api/posts", postRoute);
  app.use("/api/categories", categoryRoute);
  
app.use("/",(req,res)=>{
  res.status(200).json({message:'<b>Hello</b> welcome to my http server made with express'});
})
// Change the 404 message modifing the middleware
app.use(function(req, res, next) {
  res.status(404).json({ error: 'Sorry, that route doesn\'t exist. Have a nice day :)' });
});
app.listen(8000,() => console.log(`Backend is running. on http://localhost:${process.env.PORT}`))