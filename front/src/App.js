import React from 'react'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Topbar
 from './components/Topbar/Topbar'
 import Home from './pages/Home/Home'
// import BasicTable from './components/tables/BasicTable'
 import Single from './pages/Single/Single'
 import Login from './pages/Login/Login'
 import Register from './pages/Register/Register'
 import Settings from './pages/Settings/Settings'
 import Write from './pages/Write/Write'
const App = () => {
  const currentUser = false;
  return (
    <Router>
      <Topbar />
      <Switch>
      <Route exact path="/">
          {/*<Home />*/}
          <Home/>
        </Route>
        <Route path="/posts">
          <Home />
        </Route>
        <Route path="/register">
          {currentUser ? <Home /> : <Register />}
        </Route>
        <Route path="/login">{currentUser ? <Home /> : <Login />}</Route>
        <Route path="/post/:id"> <Single /></Route>
        <Route path="/write">{currentUser ? <Write /> : <Login />}</Route>
        <Route path="/settings">
          {currentUser ? <Settings /> : <Login />}
        </Route>
      </Switch>
    </Router>
  )
}

export default App
