import React from 'react'
import './Header.css'
const Header = () => {
    return (
        <header className="header">
            <div className="headerTitles">
                <span className="headerTitleSmall">React & Node</span>
                <span className="headerTitleLarge">Blog</span>
            </div>
            <img className="headerImg" src="https://cdn.pixabay.com/photo/2015/12/01/20/28/road-1072823_1280.jpg" alt="" />
        </header>
    )
}

export default Header
