import React from 'react'
import { Link } from "react-router-dom";
import "./Post.css"

const Post = ({p}) => {
  const PF = "http://localhost:8000/images/";
    return (
        <div className="post">
          {p.photo &&
        <img
          className="postImg"
          src={PF + p.photo}
          alt=""
        />
      }
        <div className="postInfo">
          <div className="postCats">
          {p.categories.map((c) => (
            <span className="postCat"><Link className="link" to="/posts?cat=Music">{c.name}</Link></span>
          ))}
          </div>
          <span className="postTitle">
            <Link to={`/post/${p._id}`} className="link">
              {p.title}
            </Link>
          </span>
          <hr />
          <span className="postDate">{new Date(p.createdAt).toDateString()}</span>
        </div>
        <p className="postDesc">
        {p.desc}
        </p>
      </div>
    )
}

export default Post
