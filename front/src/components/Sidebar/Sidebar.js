import React,{ useEffect, useState } from 'react'
import axios from "axios";
import { Link } from "react-router-dom";
import './Sidebar.css'
const Sidebar = () => {
    const [cats, setCats] = useState([]);

  useEffect(() => {
    const getCats = async () => {
      const res = await axios.get("/categories");
      setCats(res.data);
    };
    getCats();
  }, []);
    return (
        <aside className="sidebar">
           <div className="siderbarItem">
               <span className="sidebarTitle">About Me</span>
               <img
          src="https://themegoods-cdn-pzbycso8wng.stackpathdns.com/grandblog/demo/wp-content/uploads/2015/11/aboutme.jpg"
          alt=""
        />
               <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Debitis quaerat, eaque blanditiis nemo est iure praesentium consequatur voluptate rem reprehenderit!</p>
           </div>
           <div className="siderbarItem">
               <span className="sidebarTitle">Categories</span>
               <ul className="sidebarList">
               {cats.map((c) => (
    <Link to={`/?cat=${c.name}`} key={c._id} className="link">
    <li className="sidebarListItem">{c.name}</li>
    </Link>
  ))}
               </ul>
           </div>
           <div className="siderbarItem">
               <span className="sidebarTitle">Follow Us</span>
               <div className="siderbarSocial">
               <i className="sidebarIcon fab fa-facebook-square"></i>
                <i className="sidebarIcon fab fa-twitter-square"></i>
                <i className="sidebarIcon fab fa-pinterest-square"></i>
                <i className="sidebarIcon fab fa-instagram-square"></i>
               </div>
           </div>
        </aside>
    )
}

export default Sidebar
